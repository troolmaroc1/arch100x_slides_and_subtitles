0
00:00:00,160 --> 00:00:03,520
muito bem, então vamos falar rapidamente sobre

1
00:00:01,760 --> 00:00:05,120
"endianness", porque isso é algo que

2
00:00:03,520 --> 00:00:06,799
um pouco confuso para as pessoas que

3
00:00:05,120 --> 00:00:08,240
estão aprofundando seu o nível de conhecimento em C

4
00:00:06,799 --> 00:00:10,400
pela primeira vez

5
00:00:08,240 --> 00:00:12,559
o termo "endianness" vem do 

6
00:00:10,400 --> 00:00:14,160
livro de Jonathan Swift, As Viagens de Guliver ("Gulliver's Travels")

7
00:00:12,559 --> 00:00:16,240
um livro da época

8
00:00:14,160 --> 00:00:17,279
em que as pessoas discutiam sobre qual 

9
00:00:16,240 --> 00:00:19,760
era a maneira qual a pessoas preferiam quebrar

10
00:00:17,279 --> 00:00:21,680
ovos cozidos, se devemos começar quebrando pelo o lado menor

11
00:00:19,760 --> 00:00:23,119
ou se o lado maior

12
00:00:21,680 --> 00:00:25,000
deve ser quebrada primeiro

13
00:00:23,119 --> 00:00:26,160
isso, é claro, era uma sátira sobre as

14
00:00:25,000 --> 00:00:28,720
brigas ridículas 

15
00:00:26,160 --> 00:00:31,279
entre Inglaterra e França 

16
00:00:28,720 --> 00:00:33,520
e suas diversas maneiras de fazer determinadas coisas

17
00:00:31,279 --> 00:00:34,960
bom, o termo "little endianness" foi

18
00:00:33,520 --> 00:00:39,120
introduzido para se referir ao modo como

19
00:00:34,960 --> 00:00:41,840
você armezena valores como 0x12345678

20
00:00:39,120 --> 00:00:44,000
na memória ram, mais especificamente, "little

21
00:00:41,840 --> 00:00:46,320
endianness" significa que a menor extremidade, ou seja,

22
00:00:44,000 --> 00:00:47,039
o byte menos significativo

23
00:00:46,320 --> 00:00:49,280
será armazenado

24
00:00:47,039 --> 00:00:50,239
no endereço mais baixo, então, o valor 0x123

25
00:00:49,280 --> 00:00:54,000
45678

26
00:00:50,239 --> 00:00:57,039
será armazenado como 0x78

27
00:00:54,000 --> 00:01:00,559
no menor endereço de memória, e em sequência os valores 56

28
00:00:57,039 --> 00:01:02,640
34 12. Neste caso, a arquitetura Intel (x86) é considerada "little endian" e

29
00:01:00,559 --> 00:01:05,680
consequentemente, se você olhar os valores armazenados

30
00:01:02,640 --> 00:01:07,280
na memória ram, vai parecer que eles estão de trás para frente

31
00:01:05,680 --> 00:01:09,760
em relação a maneira que seriam armazenados

32
00:01:07,280 --> 00:01:12,159
naturalmente em hexadecimal

33
00:01:09,760 --> 00:01:13,600
já "big endian" é o oposto, então

34
00:01:12,159 --> 00:01:15,680
se você armazenar o valor 0x12345678

35
00:01:13,600 --> 00:01:17,439
na memória ram, a ordenção no formato "big end" o 

36
00:01:15,680 --> 00:01:19,600
byte mais significativo será armazenado primeiro

37
00:01:17,439 --> 00:01:21,439
então, 0x12345678

38
00:01:19,600 --> 00:01:22,400
é o valor que será visto ao observar um byte 

39
00:01:21,439 --> 00:01:25,040
de cada vez

40
00:01:22,400 --> 00:01:26,159
um exemplo é o tráfego de rede, que é enviando tipicamente

41
00:01:25,040 --> 00:01:29,040
no formato "big endian",

42
00:01:26,159 --> 00:01:30,560
se você executar o comando man byteorder

43
00:01:29,040 --> 00:01:32,720
você verá o "posix"

44
00:01:30,560 --> 00:01:34,479
da função rede para host, assim como a sua ordenação dos bytes,

45
00:01:32,720 --> 00:01:37,520
também é possível ver a ordem dos bytes de rede para

46
00:01:34,479 --> 00:01:38,159
host, rede para host long, rede

47
00:01:37,520 --> 00:01:40,640
para host

48
00:01:38,159 --> 00:01:43,119
short e host para network long, host para

49
00:01:40,640 --> 00:01:45,360
rede short e assim pro diante

50
00:01:43,119 --> 00:01:46,320
a maioria dos sistemas RISC quando

51
00:01:45,360 --> 00:01:49,600
estavam começando armazenavam

52
00:01:46,320 --> 00:01:50,960
seus bytes no formato "big endian" e, eventualmente

53
00:01:49,600 --> 00:01:52,880
mudaram, passando a armazenar 

54
00:01:50,960 --> 00:01:56,079
os bytes tanto como "big endian" ou 

55
00:01:52,880 --> 00:01:58,399
"little endian", e são sistemas conhecidos como "bi-endian"

56
00:01:56,079 --> 00:01:59,200
considerando que a arquitetura arm começou utilizando o formato "little endian"

57
00:01:58,399 --> 00:02:02,960
e agora 

58
00:01:59,200 --> 00:02:03,759
utiliza o formato "bi-endian", existem algumas coisas que

59
00:02:02,960 --> 00:02:06,320
frequentemente induzem  

60
00:02:03,759 --> 00:02:08,879
as pessoas ao erro, a primeira é que a maneira de armazenar os bytes "endianness"

61
00:02:06,320 --> 00:02:11,039
só se aplica ao armazenamento de memória

62
00:02:08,879 --> 00:02:12,239
de valores e não a registradores, sempre que

63
00:02:11,039 --> 00:02:14,800
você estiver olhando para um registrador

64
00:02:12,239 --> 00:02:16,720
ele será representado com ordenação no formato "big endian"

65
00:02:14,800 --> 00:02:17,440
significando que o byte mais significativo está

66
00:02:16,720 --> 00:02:19,840
mais à esquerda

67
00:02:17,440 --> 00:02:22,160
e o byte menos significativo está a mais à direita,

68
00:02:19,840 --> 00:02:22,879
além disso, o formato de ordenação "endianness" aplica-se apenas

69
00:02:22,160 --> 00:02:25,360
a bytes

70
00:02:22,879 --> 00:02:27,040
e não a bit, frequentemente você sabe que as pessoas

71
00:02:25,360 --> 00:02:28,800
vão ficar confusas e dirão:

72
00:02:27,040 --> 00:02:30,640
eu deveria trocar a ordem dos bits

73
00:02:28,800 --> 00:02:32,000
para "endianness", e a resposta é não

74
00:02:30,640 --> 00:02:33,680
você não deve,

75
00:02:32,000 --> 00:02:36,000
quando você está olhando para a memória e não

76
00:02:33,680 --> 00:02:37,760
para os registradores e você vê os bytes

77
00:02:36,000 --> 00:02:39,200
você sempre irá ver o bit

78
00:02:37,760 --> 00:02:40,879
menos significativo

79
00:02:39,200 --> 00:02:44,480
no lado direito e o bit mais 

80
00:02:40,879 --> 00:02:46,319
significativo no lado esquerdo,

81
00:02:44,480 --> 00:02:47,920
estes foram os tipos de dados fundamentais e suas representações

82
00:02:46,319 --> 00:02:49,280
e estão descritos no manual da Intel (x86) e pode ser 

83
00:02:47,920 --> 00:02:52,000
visto seção de atualização 

84
00:02:49,280 --> 00:02:53,200
então, como você sabe, uma variável do tipo char é representado por um 

85
00:02:52,000 --> 00:02:55,680
único byte, 

86
00:02:53,200 --> 00:02:56,319
então na há problema com o formato em que será armazenado (endianness), enquanto 

87
00:02:55,680 --> 00:02:58,879
uma variável do tipo short

88
00:02:56,319 --> 00:03:00,239
é multi-byte e já irá sofrer influência, dependendo do formato em que será armazenado (endianness)

89
00:02:58,879 --> 00:03:01,519
o dado pode ser armazenado na ordem onde o byte menos

90
00:03:00,239 --> 00:03:04,239
significativo virá primeiro

91
00:03:01,519 --> 00:03:05,680
seguido dos bytes mais significativos, ou seja,

92
00:03:04,239 --> 00:03:08,000
uma ordenação "little endian", como o Intel

93
00:03:05,680 --> 00:03:10,080
ou o oposto, se utilizarmos uma ordenação no formato "big endian"

94
00:03:08,000 --> 00:03:11,200
mas, os bits dentro do byte ainda continuam

95
00:03:10,080 --> 00:03:13,360
sendo representados na ordem

96
00:03:11,200 --> 00:03:16,239
onde os bits menos significativo estão mais a direita e

97
00:03:13,360 --> 00:03:19,040
os bits mais signiticativos estão mais a esqueda

98
00:03:16,239 --> 00:03:20,879
aqui está um exemplo de como isso se parece

99
00:03:19,040 --> 00:03:22,560
durante as aulas, frequentemente eu irei 

100
00:03:20,879 --> 00:03:23,920
desenhar os endereços menores em baixo e os endereços

101
00:03:22,560 --> 00:03:25,840
maiores em cima 

102
00:03:23,920 --> 00:03:27,360
você deve ter em mente e ser flexível para poder lidar

103
00:03:25,840 --> 00:03:29,920
com outras pessoas desenhando

104
00:03:27,360 --> 00:03:31,680
as coisas em qualquer outra direção, mas 

105
00:03:29,920 --> 00:03:32,799
isso entrará na seção de pilhas

106
00:03:31,680 --> 00:03:35,360
que iremos aprender mais 

107
00:03:32,799 --> 00:03:37,280
tarde, isso é apenas uma representação mais ou menos como foi ensinado

108
00:03:35,360 --> 00:03:38,480
durante minhas aulas de arquitetura de computadores

109
00:03:37,280 --> 00:03:39,360
e é assim que eu continuo

110
00:03:38,480 --> 00:03:42,799
repassando

111
00:03:39,360 --> 00:03:45,599
então, os menores em baixo e os maiores em cima, como eu havia dito

112
00:03:42,799 --> 00:03:46,879
os registradores sempre são representados em 

113
00:03:45,599 --> 00:03:48,959
um formato "big endian"

114
00:03:46,879 --> 00:03:50,720
com o byte mais significativo mais a

115
00:03:48,959 --> 00:03:51,519
esquerda e o byte menos significativo mais 

116
00:03:50,720 --> 00:03:53,360
a direita

117
00:03:51,519 --> 00:03:55,120
então, se a ordenação é no formato "big endian" ou "little

118
00:03:53,360 --> 00:03:56,080
endian" o registrador sempre vai 

119
00:03:55,120 --> 00:03:58,239
mostrar no 

120
00:03:56,080 --> 00:04:00,000
formato "big endian", então a diferença

121
00:03:58,239 --> 00:04:02,000
entre "big ending" e "little endian"

122
00:04:00,000 --> 00:04:03,280
está na ordem em que eles são armazenados

123
00:04:02,000 --> 00:04:05,680
nos endereços de memória

124
00:04:03,280 --> 00:04:07,680
em uma arquitetura "big endian" a extremidade maior

125
00:04:05,680 --> 00:04:09,599
irá para o endereço mais baixo

126
00:04:07,680 --> 00:04:11,439
enquanto em na arquitetura "little endian"

127
00:04:09,599 --> 00:04:12,319
a extremidade menor irá para o endereço

128
00:04:11,439 --> 00:04:14,080
mais baixo

129
00:04:12,319 --> 00:04:16,160
consequentemente a ordem dos bits parece

130
00:04:14,080 --> 00:04:18,160
estar de trás para frente quendo você está olhando para ele

131
00:04:16,160 --> 00:04:19,919
na memória, agora a maneira mais comum que

132
00:04:18,160 --> 00:04:22,240
você irá ver em ferramentas

133
00:04:19,919 --> 00:04:24,639
geralmente é algo parecido com 

134
00:04:22,240 --> 00:04:27,199
a forma que escrevemos e a ordem

135
00:04:24,639 --> 00:04:27,680
que vamos ler é da esquerda para 

136
00:04:27,199 --> 00:04:30,400
a direita 

137
00:04:27,680 --> 00:04:32,560
e de cima para baixo, por exemplo, se você estiver

138
00:04:30,400 --> 00:04:34,880
olhando para os registradores, dissemos que

139
00:04:32,560 --> 00:04:36,639
a forma de representação ("endianness") é sempre no formato "big endian"

140
00:04:34,880 --> 00:04:38,880
então, você tem 112233

141
00:04:36,639 --> 00:04:41,919
e etc, o lado do menor endereço

142
00:04:38,880 --> 00:04:42,800
aqui, os bytes menos significativos

143
00:04:41,919 --> 00:04:45,120
aqui e os

144
00:04:42,800 --> 00:04:46,240
bytes mais significativos aqui, mas quando você

145
00:04:45,120 --> 00:04:48,960
pega e coloca isso

146
00:04:46,240 --> 00:04:49,840
na memória, ele será invertido, então

147
00:04:48,960 --> 00:04:52,960
1122

148
00:04:49,840 --> 00:04:54,840
33 se transforma em 8877

149
00:04:52,960 --> 00:04:57,759
66

150
00:04:54,840 --> 00:05:01,600
e etc, mas novamente isso é

151
00:04:57,759 --> 00:05:03,680
apenas para bytes na memória, se você pedir para 

152
00:05:01,600 --> 00:05:04,880
seu debugger para representar-ló em vez de

153
00:05:03,680 --> 00:05:07,039
1 byte de cada vez

154
00:05:04,880 --> 00:05:08,400
4 bytes por vez, ou até mesmo 8

155
00:05:07,039 --> 00:05:10,960
bytes por vez

156
00:05:08,400 --> 00:05:11,440
o debugger consequentemente vai invertê-lo

157
00:05:10,960 --> 00:05:14,479
e representá-lo

158
00:05:11,440 --> 00:05:15,840
no formato "big endian", então, por exemplo se nós

159
00:05:14,479 --> 00:05:16,639
tivermos o valor 0x12345

160
00:05:15,840 --> 00:05:19,440
678

161
00:05:16,639 --> 00:05:20,639
87654321

162
00:05:19,440 --> 00:05:24,080
se você exibi-lo com

163
00:05:20,639 --> 00:05:26,479
4 bytes de cada vez,

164
00:05:24,080 --> 00:05:27,759
os primeiros 4 bytes que 

165
00:05:26,479 --> 00:05:30,240
são ser mostrados no formato "little

166
00:05:27,759 --> 00:05:32,240
end" dessa forma

167
00:05:30,240 --> 00:05:34,479
87654321

168
00:05:32,240 --> 00:05:36,479
esses são os 4 bytes menos significativos

169
00:05:34,479 --> 00:05:38,960
invertidos para serem representados na ordem "big endian"

170
00:05:36,479 --> 00:05:40,720
segudios pelos próximos 4 bytes mais significativos

171
00:05:38,960 --> 00:05:41,520
invertidos para serem na ordem "big

172
00:05:40,720 --> 00:05:44,080
endian"

173
00:05:41,520 --> 00:05:46,000
e o valor 123456 

174
00:05:44,080 --> 00:05:48,240
que ainda aparenta estar de trás para frente

175
00:05:46,000 --> 00:05:49,440
em relação à interpretação do valor completo

176
00:05:48,240 --> 00:05:51,120
de 8 bytes

177
00:05:49,440 --> 00:05:52,800
se você pedir ao debugger para

178
00:05:51,120 --> 00:05:54,560
exibir o valor 8 bytes por vez, então

179
00:05:52,800 --> 00:05:56,880
você vai começar a ver tal qual

180
00:05:54,560 --> 00:05:58,479
você vê na memória

181
00:05:56,880 --> 00:05:59,360
que corresponde exatamente ao que você vê

182
00:05:58,479 --> 00:06:01,520
no registrador

183
00:05:59,360 --> 00:06:03,360
isso ocorre porque você correspondeu a forma de representação "big

184
00:06:01,520 --> 00:06:06,319
endianness" da memória com a forma de representação 

185
00:06:03,360 --> 00:06:08,319
"big endianness" do registrador

186
00:06:06,319 --> 00:06:09,440
aqui está um exemplo da mesma coisa 

187
00:06:08,319 --> 00:06:12,960
no gdb

188
00:06:09,440 --> 00:06:14,960
dessa maneira, o byte menos significativo

189
00:06:12,960 --> 00:06:16,720
se for mostrado 4 bytes por vez

190
00:06:14,960 --> 00:06:20,479
será invertido e então

191
00:06:16,720 --> 00:06:23,600
0x54889e5 0x548

192
00:06:20,479 --> 00:06:24,960
89e5

193
00:06:23,600 --> 00:06:26,560
isso foi só uma breve introdução sobre

194
00:06:24,960 --> 00:06:27,840
as formas de representação ("endianness"), você realmente 

195
00:06:26,560 --> 00:06:29,120
só precisa saber sobre isso quando

196
00:06:27,840 --> 00:06:32,400
estiver olhando para

197
00:06:29,120 --> 00:06:34,000
valores em memória ou

198
00:06:32,400 --> 00:06:36,319
precisar fazer incrementos que são menores

199
00:06:34,000 --> 00:06:38,160
que o tamanho de uma instância de um registrador específico

200
00:06:36,319 --> 00:06:41,759
que vai ler

201
00:06:38,160 --> 00:06:41,759
escrever o valor

